package main

import (
	"fmt"
	"log"
	"net/http"
	"strconv"
	"strings"
)

func between(value string, a string, b string) string {
	// Get substring between two strings.
	posFirst := strings.Index(value, a)
	if posFirst == -1 {
		return ""
	}
	posLast := strings.Index(value, b)
	if posLast == -1 {
		return ""
	}
	posFirstAdjusted := posFirst + len(a)
	if posFirstAdjusted >= posLast {
		return ""
	}
	return value[posFirstAdjusted:posLast]
}

func toRomanian(text string) string {
	var romanian = []string{"L", "XL", "X", "IX", "V", "IV", "I"}
	var galactic = []string{"tegj", "pishtegj", "pish", "globpish", "prok", "globprok", "glob"}
	text = strings.TrimSpace(text)
	stringSlice := strings.Split(text, " ")
	result := ""
	for k, v := range stringSlice {
		fmt.Println("stringSlice value:", v, "stringSlice key:", k)
		temp := 0
		for i := range galactic {
			if galactic[i] == v {
				temp = i
				// fmt.Println("galactic value:", galactic[i], "galactic key:", i)
				result = result + romanian[temp]
			}
		}

	}
	return result
}

func toDecimal(roman string) int {
	var romanian = []string{"M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I"}
	var arabic = []int{1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1}

	result := 0
	for {

		for k, v := range romanian {

			if (strings.Index(roman, v)) == 0 {

				result = result + arabic[k]
				fmt.Println("roman value:", roman, "romanian value:", v, "romanian key:", k)
				// roman = strings.TrimLeft(roman, v)
				roman = strings.Replace(roman, v, "", 1)
			}
		}
		if roman == "" {
			break
		}

	}

	return result
}

func convert(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path != "/" {
		http.Error(w, "404 not found.", http.StatusNotFound)
		return
	}

	switch r.Method {
	case "GET":
		http.ServeFile(w, r, "form.html")
	case "POST":
		// Call ParseForm() to parse the raw query and update r.PostForm and r.Form.
		if err := r.ParseForm(); err != nil {
			fmt.Fprintf(w, "ParseForm() err: %v", err)
			return
		}
		input := r.FormValue("input")
		out := between(input, "is", "?")
		roman := toRomanian(out)
		decimal := toDecimal(roman)
		if decimal == 0 {
			fmt.Fprintf(w, "I have no idea what you are talking about")
		} else {
			lowOut := strings.ToLower(out)
			car := ""
			if strings.Index(lowOut, "silver") >= 0 {
				decimal = decimal * 17
				car = " Credits"
			} else if strings.Index(lowOut, "sold") >= 0 {
				decimal = decimal * 14450
				car = " Credits"
			} else if strings.Index(lowOut, "iron") >= 0 {
				decimal = decimal * 391 / 2
				car = " Credits"
			}
			fmt.Fprintf(w, out+" is "+strconv.Itoa(decimal)+car)
		}

		// fmt.Fprintf(w, roman)
		// fmt.Fprintf(w, " -> ")
		// fmt.Fprintf(w, strconv.Itoa(decimal))

	default:
		fmt.Fprintf(w, "Sorry, only GET and POST methods are allowed")
	}
}

func main() {
	http.HandleFunc("/", convert)

	fmt.Printf("Starting server...\n")
	if err := http.ListenAndServe(":8080", nil); err != nil {
		log.Fatal(err)
	}
}
